#!/usr/bin/env runhaskell

main = do
  print (pyths1 50)

pyths1 :: Int -> [(Int, Int, Int)]
pyths1 n =
  [(x,y,z) | x <- [1..n], y <- [1..n], z <- [1..n], x^2 + y^2 == z^2]

pyths2 :: Int -> [(Int, Int, Int)]
pyths2 n =
  [(x,y,z) | x <- [1..n], y <- [x..n], z <- [1..n], x^2 + y^2 == z^2]


pyths3 :: Int -> [(Int, Int, Int)]
pyths3 n =
  [(x,y,z) | x <- [1..n], y <- [x..n], z <- [y..n], x^2 + y^2 == z^2]

