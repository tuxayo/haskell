#!/usr/bin/env runhaskell


main = do
  print (scalarProduct [1,2,3] [4,5,6])
  print (scalarProduct2 [1,2,3] [4,5,6])
  print (scalarProductRecursive [1,2,3] [4,5,6])


scalarProduct xs ys =
  sum (map (\(x,y) -> x*y) (zip xs ys))

-- nice
scalarProduct2 xs ys =
  sum [x*y | (x,y) <- zip xs ys]

scalarProductRecursive (x:xs) (y:ys)
  | xs == [] = x*y
  | ys == [] = x*y
  | otherwise = x*y + scalarProductRecursive xs ys
