#!/usr/bin/env runhaskell

main = do
  print (perfects 8200)


perfects :: Int -> [Int]
perfects n = [x | x <- [1..n], isPerfect x]


isPerfect :: Int -> Bool
isPerfect n =
  n == (sum (take all_but_last (factors n)))
  where
    all_but_last = length (factors n) -1


factors n = [ x | x <- [1..n], n `mod` x == 0 ]
