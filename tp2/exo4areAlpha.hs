#!/usr/bin/env runhaskell

--main = do --TODO find why it doesn't work
--    print (isEmpty list) 
--    print (isEmpty [])

import Data.Char(isAlpha)

areAlpha :: String -> Bool
areAlpha xs = if xs == [] then
                True
              else if isAlpha (head xs)
                   then
                     areAlpha (tail xs)
                   else
                     False

areAlphaGuard :: String ->Bool
areAlphaGuard xs
  | xs == [] = True
  | isAlpha (head xs) = areAlphaGuard (tail xs)
  | otherwise = False

areAlphaPatternMatching :: String ->Bool
areAlphaPatternMatching [] = True
areAlphaPatternMatching (x:xs)
  | isAlpha (x) = areAlphaPatternMatching xs
  | otherwise = False

areAlphaLogical :: String -> Bool
areAlphaLogical xs =
  (xs == []) ||
  (isAlpha (head xs)) && areAlphaLogical (tail xs)

areAlphaMapAnd :: String -> Bool
areAlphaMapAnd xs =
  and (map (\x -> isAlpha x) xs)
