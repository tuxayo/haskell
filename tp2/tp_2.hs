-- exo1-qsort.hs ##############################

main = do
    print (42) 
    print (list)

list = [1, 2, 3, 4, 5]

qsort [] = []
qsort (x:xs) =
  qsort smaller ++ [x] ++ qsort larger
  where
    smaller = [a | a <- xs, a < x]
    larger = [b | b <- xs, x < b]
#!/usr/bin/env runhaskell

--main = do --TODO find why it doesn't work
--    print (isEmpty list) 
--    print (isEmpty [])

list = [1, 2, 3, 4, 5]

-- exo 2 ################################################
isEmptyCond :: Eq a => [a] -> Bool
isEmptyCond list =
  if list == [] then
    True
  else
    False

isEmptyFilter :: [a] -> Bool
isEmptyFilter [] = True
isEmptyFilter _ = False

isEmpty3 :: [a] -> Bool
isEmpty3 list =
  length list == 0


-- exo 3 ####################################################
safeTailCond :: [a] -> [a]
safeTailCond list =
  if isEmptyFilter list then
    []
  else
    tail list

safeTailGuard :: Eq a => [a] -> [a]
safeTailGuard list
  | list == [] = []
  | otherwise = tail list

safeTailPattern :: [a] -> [a]
safeTailPattern [] = []
safeTailPattern list = tail list

-- exo4areAlpha.hs ##################################################
#!/usr/bin/env runhaskell

--main = do --TODO find why it doesn't work
--    print (isEmpty list) 
--    print (isEmpty [])

import Data.Char(isAlpha)

areAlpha :: String -> Bool
areAlpha xs = if xs == [] then
                True
              else if isAlpha (head xs)
                   then
                     areAlpha (tail xs)
                   else
                     False

areAlphaGuard :: String ->Bool
areAlphaGuard xs
  | xs == [] = True
  | isAlpha (head xs) = areAlphaGuard (tail xs)
  | otherwise = False

areAlphaPatternMatching :: String ->Bool
areAlphaPatternMatching [] = True
areAlphaPatternMatching (x:xs)
  | isAlpha (x) = areAlphaPatternMatching xs
  | otherwise = False

areAlphaLogical :: String -> Bool
areAlphaLogical xs =
  (xs == []) ||
  (isAlpha (head xs)) && areAlphaLogical (tail xs)

areAlphaMapAnd :: String -> Bool
areAlphaMapAnd xs =
  and (map (\x -> isAlpha x) xs)

-- exo5_lamba.hs ################################################
carre :: Num a => a -> a
carre x = x^2
moyenne :: Fractional a => [a] -> a
moyenne ns = (sum ns) / (fromIntegral (length ns))
norme :: Floating a => [a] -> a
norme ns = sqrt (moyenne (map carre ns))

--main = print (norme [1..5])
main = print ((\ns -> sqrt ((\ns -> (sum ns) / (fromIntegral (length ns))) (map (\x -> x^2) ns))) [1..5])

-- exo6_produit_scalaire.hs ################################################

main = do
  print (scalarProduct [1,2,3] [4,5,6])
  print (scalarProduct2 [1,2,3] [4,5,6])
  print (scalarProductRecursive [1,2,3] [4,5,6])


scalarProduct xs ys =
  sum (map (\(x,y) -> x*y) (zip xs ys))

-- nice
scalarProduct2 xs ys =
  sum [x*y | (x,y) <- zip xs ys]

scalarProductRecursive (x:xs) (y:ys)
  | xs == [] = x*y
  | ys == [] = x*y
  | otherwise = x*y + scalarProductRecursive xs ys

-- exo7_perfect_numbers.hs #################################################
main = do
  print (perfects 8200)


perfects :: Int -> [Int]
perfects n = [x | x <- [1..n], isPerfect x]


isPerfect :: Int -> Bool
isPerfect n =
  n == (sum (take all_but_last (factors n)))
  where
    all_but_last = length (factors n) -1


factors n = [ x | x <- [1..n], n `mod` x == 0 ]

-- exo8_pythagorean_triple.hs ###############################################
main = do
  print (pyths1 50)

pyths1 :: Int -> [(Int, Int, Int)]
pyths1 n =
  [(x,y,z) | x <- [1..n], y <- [1..n], z <- [1..n], x^2 + y^2 == z^2]

pyths2 :: Int -> [(Int, Int, Int)]
pyths2 n =
  [(x,y,z) | x <- [1..n], y <- [x..n], z <- [1..n], x^2 + y^2 == z^2]


pyths3 :: Int -> [(Int, Int, Int)]
pyths3 n =
  [(x,y,z) | x <- [1..n], y <- [x..n], z <- [y..n], x^2 + y^2 == z^2]

