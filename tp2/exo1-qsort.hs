#!/usr/bin/env runhaskell

main = do
    print (42) 
    print (list)

list = [1, 2, 3, 4, 5]

qsort [] = []
qsort (x:xs) =
  qsort smaller ++ [x] ++ qsort larger
  where
    smaller = [a | a <- xs, a < x]
    larger = [b | b <- xs, x < b]
    
