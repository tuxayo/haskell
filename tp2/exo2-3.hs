#!/usr/bin/env runhaskell

--main = do --TODO find why it doesn't work
--    print (isEmpty list) 
--    print (isEmpty [])

list = [1, 2, 3, 4, 5]

-- exo 2
isEmptyCond :: Eq a => [a] -> Bool
isEmptyCond list =
  if list == [] then
    True
  else
    False

isEmptyFilter :: [a] -> Bool
isEmptyFilter [] = True
isEmptyFilter _ = False

isEmpty3 :: [a] -> Bool
isEmpty3 list =
  length list == 0


-- exo 3
safeTailCond :: [a] -> [a]
safeTailCond list =
  if isEmptyFilter list then
    []
  else
    tail list

safeTailGuard :: Eq a => [a] -> [a]
safeTailGuard list
  | list == [] = []
  | otherwise = tail list

safeTailPattern :: [a] -> [a]
safeTailPattern [] = []
safeTailPattern list = tail list
