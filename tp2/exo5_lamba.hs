#!/usr/bin/env runhaskell

carre :: Num a => a -> a
carre x = x^2
moyenne :: Fractional a => [a] -> a
moyenne ns = (sum ns) / (fromIntegral (length ns))
norme :: Floating a => [a] -> a
norme ns = sqrt (moyenne (map carre ns))

--main = print (norme [1..5])
main = print ((\ns -> sqrt ((\ns -> (sum ns) / (fromIntegral (length ns))) (map (\x -> x^2) ns))) [1..5])
