#!/usr/bin/env runhaskell

main = do
    let extract_in_tuple = do
        snd(fst (("foo", 4), True))
    --print extract_in_tuple

    let split_list_in_tuple list = do
        (head list, tail list)
    --print (split_list_in_tuple [1, 2, 3])

    let fifth_of_list list = do
        head(tail(tail(tail(tail list))))
    print (fifth_of_list [1,2,3,4,5])
