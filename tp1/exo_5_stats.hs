list = [1, 2, 3, 4, 5]

main = do
    print (mean list) 
    print (meanAbsoluteDiff list)


mean list = sum list / fromIntegral (length list)



meanAbsoluteDiff list = mean (map (difference (mean list)) list)
   where
     difference a b = abs (a - b)
