#!/usr/bin/env runhaskell

main = do
  print (add (1, 2) (3, 4))
  print (multiply (1, 2) (3, 4))
  print (divide (1, 2) (3, 4))
  print (add (multiply (0, 1) (1, 1)) (1, -1))
  print (multiply (1, 1) (2, 3))

add :: (Num t1, Num t2) => (t1, t2) -> (t1, t2) -> (t1, t2)
add (a, b) (c, d) = do
  ((a+c),(b+d))

multiply :: Num t => (t, t) -> (t, t) -> (t, t)
multiply (a, b) (c, d) = do
  ((a*c - b*d), (b*c + a*d))

divide :: Fractional t => (t, t) -> (t, t) -> (t, t)
divide (a, b) (c, d) = do
  ( (a*c+b*d)/(c^2+d^2), (b*c-a*d)/(c^2+d^2) )
