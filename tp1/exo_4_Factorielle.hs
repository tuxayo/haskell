module Factorielle (fact) where

    main = print (fact 50)

    fact = fromIntegral . factProduct -- . to compose fonctions

    factRec 0 = 1
    factRec n = n * factRec (n - 1)

    factProduct n = product [1..n]
