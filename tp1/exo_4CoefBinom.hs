import Factorielle

binom n k = fact n `div` (fact k * fact (n-k))

main = print (binom 8 3)
