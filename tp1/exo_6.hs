#!/usr/bin/env runhaskell

second xs = head ( tail xs )
swap (x , y ) = (y , x )
double = mult 2
palindrome xs = reverse xs == xs
-- functions with two args:
twice f x = f ( f x )
pair x y = (x , y )
mult x y = x * y
