# ressources:
https://www.fpcomplete.com/school/to-infinity-and-beyond/pick-of-the-week/haskell-fast-hard
http://stackoverflow.com/questions/1012573/getting-started-with-haskell
http://learnyouahaskell.com/chapters
http://learnxinyminutes.com/docs/haskell/
http://stackoverflow.com/questions/3077866/large-scale-design-in-haskell?rq=1
IRC:
  - #haskell-beginners
  - #haskell

https://hackage.haskell.org/package/base/docs/Prelude.html
https://hackage.haskell.org/package/base/docs/Debug-Trace.html


# Parsing Stuff in Haskell - Monads and Applicatives usage
https://www.youtube.com/watch?v=r_Enynu_TV0

# packages
cabal install cabal-uninstall
ghc-mod # dependency of an atom package
haskell-stylish-haskell # dependency of an atom package
hlint
hunit
