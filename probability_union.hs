-- motivation: calculate uptime of a *set* of DNS set from
-- https://www.opennicproject.org/

unionTwoProb probA probB =
    probA + not_A * probB
    where
        not_A = 1 - probA

unionProbs :: Num a => [a] -> a
unionProbs =
    foldl unionTwoProb 0
