import System.IO

board = [5,4,3,2,1] :: [Int] -- number of stars per line

nim :: IO()
nim = do
  winner <- mainLoop 1 board
  putStrLn (playerToStr winner ++ " won")


playerToStr player = "player " ++ (show player)
togglePlayer 1 = 2
togglePlayer 2 = 1


mainLoop :: Int -> [Int] -> IO Int
mainLoop player [0,0,0,0,0] = return (togglePlayer player)
mainLoop player board = do
  putStrLn("Turn for " ++ playerToStr player)
  putStrLn (show board)
  move <- getMove
  (nextPlayer,newBoard) <- makeTurn board move player
  mainLoop nextPlayer newBoard


getMove :: IO (Int,Int)
getMove = do
  putStrLn "Which line?"
  line <- getLine
  putStrLn "How many stars?"
  stars <- getLine
  return (read line,read stars)


-- apply move if it's valid and return updated board and next player
makeTurn :: [Int] -> (Int,Int) -> Int -> IO (Int, [Int])
makeTurn board move player =
  if moveIsValid board move then
    let newBoard = updateGame board move in
    do
      print newBoard
      return (togglePlayer player, newBoard)
  else
    do
      putStrLn "**Invalid move!**"
      return (player, board)


estChoixValide = moveIsValid
moveIsValid :: [Int] -> (Int,Int) -> Bool
moveIsValid board (line,stars)
  | stars < 1 = False
  | line < 1 || line > (length board) = False
  | stars_in_chosen_line < stars = False
  | otherwise = True
  where
    stars_in_chosen_line =  board !! (line-1)


mettreAJour = updateGame
updateGame :: [Int] -> (Int,Int) -> [Int]
updateGame board (line,stars) =
  firstLines ++ updatedLine ++ lastLines
  where
    firstLines = take (line-1) board
    updatedLine = [starsInChosenLine - stars]
    lastLines = drop line board
    starsInChosenLine =  board !! (line-1)
