type Pixel = Int
type Ligne = [Pixel]
type Image = [Ligne]
type Voisinage = [Pixel]
type Effet = (Pixel,Voisinage) -> Pixel
type Point = (Int,Int)

taille :: Image -> (Int,Int)
taille image = (x,y)
    where
      x = (length . head) image
      y = length image

sousliste :: Int -> Int -> [a] -> [a]
sousliste i j xs = drop (i-1) (take j xs) -- i: fin, j: début

voisinage :: Image -> Point -> (Pixel,Voisinage)
voisinage img (x,y) = (p,ps)
    where
      p = (img !! (y-1)) !! (x-1)
      ps = concat (map (sousliste (x-1) (x+1)) (sousliste (y-1) (y+1) img))

appliquerEffet :: Effet -> Image -> Image
appliquerEffet effet image = 
    let
        (xmax,ymax) = taille image
        pts = [ [ (x,y) | x <- [1..xmax] ]  |  y <- [1..ymax] ]
    in
      map (map (\p -> effet (voisinage image p))) pts


showImage :: Image -> String
showImage image = concat stringsPlusNewLine
  where
    stringsPlusNewLine = map (\line -> line ++ ['\n']) imgToStrings
    imgToStrings = map (map pixelToChar) image

showImage2 :: Image -> String
showImage2 image = concat stringsPlusNewLine
  where
    stringsPlusNewLine = map (\line -> lineToString line ++ ['\n']) image
    lineToString line = map pixelToChar line

pixelToChar 0 = ' '
pixelToChar 1 = '*'


smooth :: Effet
smooth (pixel, voisinage)
  | voisinsNoirs > deuxTiers = 1
  | voisinsBlancs > deuxTiers = 0
  | otherwise = pixel
  where
    voisinsBlancs = nbVoisins - voisinsNoirs
    voisinsNoirs = sum voisinage - pixel
    deuxTiers = floor (fromIntegral nbVoisins * (2/3))
    nbVoisins = length voisinage - 1


image1,image2 :: Image
image1 = [
 [1,1,1,0],
 [0,0,1,1],
 [1,0,1,0],
 [0,0,0,1]]
image2 = [
 [0,1,1,0,0,1,0,0,0,1],
 [1,0,0,1,0,1,1,0,1,1],
 [1,0,0,1,0,1,0,1,0,1],
 [0,1,1,0,0,1,0,0,0,1]]
